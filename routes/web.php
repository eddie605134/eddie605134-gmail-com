<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('index');

Route::get('/news', 'FrontController@news')->name('news');

Route::get("/news/{id}",'FrontController@detail');

Route::get('/product', 'FrontController@product');

Route::get('/product_details/{product_id}', 'FrontController@product_details');

//購物車

Route::post('/add_cart/{product_id}', 'FrontController@add_cart');

Route::get('/cart', 'FrontController@cart_total');

Route::get('/contactus', 'FrontController@contactus');

Route::post('/contactus/store', 'FrontController@contactus_store');




//產品頁面

Auth::routes();

Route::group(['middleware' => ['auth'],'prefix'=>'/home'], function () {

    //首頁
    Route::get('/', 'HomeController@index');


    //最新消息管理
    Route::get('/news', 'NewsController@index');

    Route::get('/news/create', 'NewsController@create');
    Route::post('/news/store', 'NewsController@store');

    Route::get('/news/edit/{id}', 'NewsController@edit');
    Route::post('/news/update/{id}', 'NewsController@update');

    Route::post('/news/delete/{id}', 'NewsController@destroy');

    //產品類型管理
    Route::get('/producttype', 'ProductTypeController@index');

    Route::get('/producttype/create', 'ProductTypeController@create');
    Route::post('/producttype/store', 'ProductTypeController@store');

    Route::get('/producttype/edit/{id}', 'ProductTypeController@edit');
    Route::post('/producttype/update/{id}', 'ProductTypeController@update');

    Route::post('/producttype/delete/{id}', 'ProductTypeController@destroy');


    //產品管理
    Route::get('/product', 'ProductController@index');

    Route::get('/product/create', 'ProductController@create');
    Route::post('/product/store', 'ProductController@store');

    Route::get('/product/edit/{id}', 'ProductController@edit');
    Route::post('/product/update/{id}', 'ProductController@update');

    Route::post('/product/delete/{id}', 'ProductController@destroy');



    //AJAX
    Route::post('/ajax_delete_new_imgs', 'NewsController@ajax_delete_new_imgs');
    Route::post('/ajax_post_sort', 'NewsController@ajax_post_sort');

});



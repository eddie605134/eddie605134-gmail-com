<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'producttypes';
    protected $fillable =['id','type_name','sort'];

    public function producttype_class()
    {
        return $this->hasMany('App\Produc','type_name','product_type')->orderby('sort','desc');

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable =['id','img','title','content','sort'];

    public function news_imgs()
    {
        return $this->hasMany('App\news_img')->orderby('sort','desc');
        //hasMany裏頭連接的是其他的子MODEL


        // $table->bigIncrements('id');
        //     $table->integer('news_id');
        //     $table->longText('img_url');
        //     $table->integer('sort')->default(0);
        //     $table->timestamps();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{

    protected $table = 'contactus';

    protected $fillable = ['id', 'name', 'email', 'message', 'created_at','updated_at'];

}

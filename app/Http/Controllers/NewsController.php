<?php

namespace App\Http\Controllers;

use App\News;
use App\News_img;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class NewsController extends Controller
{
    public function index()
    {
        $all_news = News::all();

        return view('admin/news/index', compact('all_news'));
    }

    public function create()
    {
        return view('admin/news/create');
    }

    public function store(Request $request)
    {
        $news_data = $request->all();

       
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $path = $this->fileUpload($file, 'product');
            $news_data['img'] = $path;

        }

        $news_new = News::create($news_data);

        //多張圖片上傳
        $files = $request->file('news_imgs');
        if ($request->hasFile('news_imgs')) {
            foreach ($files as $file) {
                $path = $this->fileUpload($file, 'product');

                $news_imgs = new News_img;
                $news_imgs->news_id = $news_new->id;
                $news_imgs->img_url = $path;
                $news_imgs->save();

            }
        }

        return redirect('/home/news');
    }

    public function edit($id)
    {
        //$news = News::where('id','=',$id)->first();
        $news = News::with('news_imgs')->find($id);

        return view('admin/news/edit', compact('news'));
    }

    public function update(Request $request, $id)
    {
        // News::find($id)->update($request->all());

        $requsetData = $request->all();

        $item = News::find($id);

        if ($request->hasFile('img')) {

            $old_image = $item->img;
            File::delete(public_path() . $old_image);

            $file = $request->file('img'); //file 是路徑
            $path = $this->fileUpload($file, 'product');
            $requsetData['img'] = $path;
        }

        $item->update($requsetData);
        // ~~~~~~~~~多筆資料update~~~~~~~~~~~~~
        $files = $request->file('news_imgs');
        if ($request->hasFile('news_imgs')) {
            foreach ($files as $file) {
                $path = $this->fileUpload($file, 'product');

                $news_imgs = new News_img;
                $news_imgs->news_id = $item->id;
                $news_imgs->img_url = $path;
                $news_imgs->save();

            }
        }

        return redirect('/home/news');

    }

    public function destroy($id)
    {
        // News::find($id)->delete();   刪資料庫欄位

        $item = News::find($id);
        $old_image = $item->img;
        if (file_exists(public_path() . $old_image)) {
            File::delete(public_path() . $old_image);
        }
        $item->delete();
        //把路徑資料也刪掉

        //多筆資料刪除
        $news_imgs = News_img::where('news_id',$id)->get();
        foreach($news_imgs as $news_img){

            $old_image = $news_img->img_url;
            if (file_exists(public_path() . $old_image)) {
                File::delete(public_path() . $old_image);
            }
            $news_img->delete();
        }


        return redirect('/home/news');

    }

    private function fileUpload($file, $dir)
    {
        //防呆：資料夾不存在時將會自動建立資料夾，避免錯誤
        if (!is_dir('upload/')) {
            mkdir('upload/');
        }
        //防呆：資料夾不存在時將會自動建立資料夾，避免錯誤
        if (!is_dir('upload/' . $dir)) {
            mkdir('upload/' . $dir);
        }
        //取得檔案的副檔名
        $extension = $file->getClientOriginalExtension();
        //檔案名稱會被重新命名
        $filename = strval(time() . md5(rand(100, 200))) . '.' . $extension;
        //移動到指定路徑
        move_uploaded_file($file, public_path() . '/upload/' . $dir . '/' . $filename);
        //回傳 資料庫儲存用的路徑格式
        return '/upload/' . $dir . '/' . $filename;
    }

    public function ajax_delete_new_imgs(Request $request)
    {
        $newsimgid = $request->newsimgid;

        $item = News_img::find($newsimgid);
        $old_image = $item->img_url;
        if (file_exists(public_path() . $old_image)) {
            File::delete(public_path() . $old_image);
        }
        $item->delete();

        return "delete";
    }

    public function ajax_post_sort(Request $request)
    {
        $news_imgs_id =  $request->id;
        $sort = $request->sort;
        $img = News_img::find($news_imgs_id);
        //裏頭id是ajax_post_sort   data鍵值
        $img->sort = $sort;
        //裏頭sort是ajax_post_sort   data鍵值
        $img->save();

        return"hello";



    }

}

<?php

namespace App\Http\Controllers;

use App\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    public function index()
    {
        $all_producttype = ProductType::all();
        return view('admin/producttype/index',compact('all_producttype'));
    }

    public function create()
    {
        return view('admin/producttype/create');
    }

    public function store(Request $request)
    {
        $producttype_data = $request->all();

        ProductType::create($producttype_data);



        return redirect('/home/producttype');
    }

    public function edit($id)
    {

        $prouducttype = ProductType::find($id);

        return view('admin/producttype/edit', compact('prouducttype'));
    }

    public function update(Request $request, $id)
    {
        // News::find($id)->update($request->all());

        $requsetData = $request->all();

        $item = ProductType::find($id);



        $item->update($requsetData);
        // ~~~~~~~~~多筆資料update~~~~~~~~~~~~~


        return redirect('/home/producttype');

    }

    public function destroy($id)
    {
        // News::find($id)->delete();   刪資料庫欄位

        $item = ProductType::find($id);

        $item->delete();
        //把路徑資料也刪掉

        return redirect('/home/producttype');

    }
}

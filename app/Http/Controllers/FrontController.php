<?php

namespace App\Http\Controllers;

use App\News;
use App\Product;
use App\ContactUs;
use App\Mail\OrderShipped;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class FrontController extends Controller
{
    public function index()
    {
        return view('front/index');
    }
    public function news()
    {
        $news_data = News::orderby('sort', 'desc')->get();
        return view('front/news', compact('news_data'));
    }


    public function detail($id)
    {
        //寫法二 $news_imgs = News::find($id)->news_imgs;
        $news_imgs = News::with('news_imgs')->find($id); //News是MODEL
        return view('front/detail', compact('news_imgs'));
    }

    public function product()
    {
        $product_data = Product::orderby('sort', 'desc')->get();
        return view('front/product',compact('product_data'));
    }

    public function product_details($ProductId)
    {
        $Product = Product::find($ProductId);
        return view('front/product_details',compact('Product'));
    }

    public function add_cart($ProductId)
    {

        $Product = Product::find($ProductId); // assuming you have a Product model with id, name, description & price
        $rowId = $ProductId; // generate a unique() row ID

// add the product to cart

        \Cart::add(array(

            'id' => $rowId,
            'name' => $Product->title,
            'price' => $Product->price,
            'quantity' => 4,
            'attributes' => array(),
            'associatedModel' => $Product,
        ));

        return redirect('/cart');
    }

    public function cart_total()
    {

        // $userID = Auth::user()->id;
        $items = \Cart::getContent();

        return view('front/cart', compact('items'));
    }

    public function contactus()
    {
        return view('front/contactUs');
    }

    public function contactus_store(Request $request)
    {
        $news_data = $request->all();



        $contactus = ContactUs::create($news_data);

        Mail::to($request->email)->send(new OrderShipped($contactus));

        return redirect('/contactus');
    }
}

<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    public function index()
    {
        $all_product = Product::with('product_class')->get();

        return view('admin/product/index',compact('all_product'));
    }

    public function create()
    {
        $all_producttype = ProductType::all();
        //直接引用ProductType進來
        return view('admin/product/create',compact('all_producttype'));
    }

    public function store(Request $request)
    {
        $product_data = $request->all();


        if ($request->hasFile('p_img')) {
            $file = $request->file('p_img');
            $path = $this->fileUpload($file, 'p_product');
            $product_data['p_img'] = $path;

        }

        Product::create($product_data);




        return redirect('/home/product');
    }

    public function edit($id)
    {
        $producttype = ProductType::all();
        $product = Product::find($id);



        return view('admin/product/edit', compact('product','producttype'));
    }

    public function update(Request $request, $id)
    {
        // News::find($id)->update($request->all());

        $requsetData = $request->all();

        $item = Product::find($id);

        if ($request->hasFile('p_img')) {

            $old_image = $item->img;
            File::delete(public_path() . $old_image);

            $file = $request->file('p_img'); //file 是路徑
            $path = $this->fileUpload($file, 'p_product');
            $requsetData['p_img'] = $path;
        }

        $item->update($requsetData);
        // ~~~~~~~~~多筆資料update~~~~~~~~~~~~~


        return redirect('/home/product');

    }

    public function destroy($id)
    {
        // News::find($id)->delete();   刪資料庫欄位

        $item = Product::find($id);
        $old_image = $item->p_img;
        if (file_exists(public_path() . $old_image)) {
            File::delete(public_path() . $old_image);
        }
        $item->delete();
        //把路徑資料也刪掉




        return redirect('/home/product');

    }





    private function fileUpload($file, $dir)
    {
        //防呆：資料夾不存在時將會自動建立資料夾，避免錯誤
        if (!is_dir('upload/')) {
            mkdir('upload/');
        }
        //防呆：資料夾不存在時將會自動建立資料夾，避免錯誤
        if (!is_dir('upload/' . $dir)) {
            mkdir('upload/' . $dir);
        }
        //取得檔案的副檔名
        $extension = $file->getClientOriginalExtension();
        //檔案名稱會被重新命名
        $filename = strval(time() . md5(rand(100, 200))) . '.' . $extension;
        //移動到指定路徑
        move_uploaded_file($file, public_path() . '/upload/' . $dir . '/' . $filename);
        //回傳 資料庫儲存用的路徑格式
        return '/upload/' . $dir . '/' . $filename;
    }
}

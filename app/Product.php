<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable =['id','product_type','p_img','sort','title','price',];


    public function product_class()
    {
        return $this->belongsTo('App\ProductType','product_type')->orderby('sort','desc');

    }
}

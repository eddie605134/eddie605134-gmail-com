@extends('layouts/nav')



@section('css')
<style>
    .content-left {
        width: 606px;
        min-height: 500px;
        box-sizing: border-box;
        padding: 48px 48px 40px;
        margin-bottom: 60px;
        background: #fafafa;
    }

    .product_title {
        font-size: 25px;
    }

    .product_name {
        font-size: 20px;
    }

    .many {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        min-height: 58px;
    }

    .color {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        min-height: 58px;
    }

    .cart-buttons {
        padding: 10px 20px;
        width: 160px;
        min-height: 58px;
        line-height: 58px;
        font-size: 16px;
        margin: 5px 5px;
        color: #757575;
        text-align: center;
        border: 1px solid #eee;
        background-color: #fff;
        user-select: none;
        transition: opacity, border .2s linear;

    }
    /* .specification .cart-buttons{
        text-align: unset;
    } */

    .cart-buttons.active {
        border: 1px solid red;
    }

    .specification.active {
        border: 1px solid red;
    }
</style>

@endsection


@section('content')

<section class="engine"><a href="https://mobirise.info/x">css templates</a></section>
<section class="features3 cid-rRF3umTBWU" id="features3-7">



    <div class="container" style="margin-top:80px;">
        <div class="row">
            <div class="col-6"></div>



            <div class=" col-6 content-left">
            <div class="product_title">{{$Product->title}}</div>
                <div class="product_name">6GB+64GB, 冰翡翠</div>
                <div class="price">NT$6,599</div>
                <hr>
                <div class="rank">該商品可享受雙倍積分</div>
                <hr>
                <div>容量</div>
                <div class="specification col-6">
                    <div class="many">
                        <div class="cart-buttons" data-specification="6G+64GB">
                            <div class="">6G+64GB</div></div>
                        <div class="cart-buttons" data-specification="6GB+128GB">
                            <div class="">6GB+128GB</div></div>
                    </div>
                </div>
                <div>顏色</div>
                <div class="color col-6">
                    <div class="cart-buttons" data-color="紅">
                        <div class="col-4">紅</div></div>
                    <div class="cart-buttons" data-color="黃">
                        <div class="col-4">黃</div></div>
                    <div class="cart-buttons" data-color="青">
                        <div class="col-4">青</div></div>
                    <div class="cart-buttons" data-color="紫">
                        <div class="col-4">紫</div></div>
                </div>

            <form action="/add_cart/{{$Product->id}}" method="post">
                    @csrf
                    <div class="quantity">
                        <div>數量</div>
                        <div id="field1">
                            <button type="button" id="sub" class="sub">-</button>
                            <input type="number" id="1" value="1" name="number" />
                            <button type="button" id="add" class="add">+</button>
                        </div>
                    </div>
                    <div class="total">
                        <div class="top_total row">
                            <div> <span>Redmi Note 8 Pro</span> <span>冰翡翠</span><span>6GB+64GB</span>* <span>1</span>
                            </div>
                            <div>NT${{$Product->price}}</div>
                        </div>
                        <div class="botton_total row">
                            <div>總計：</div>
                            <div>NT$6,599</div>
                        </div>
                    </div>
                    <div>
                        <input type="product" name="title" id="title" value="{{$Product->id}}" hidden>
                        <input type="text" name="specification" id="specification" value="6GB+64GB" hidden>
                        <input type="text" name="color" id="color" value="黃" hidden>
                    </div>
                    <button type="submit" class="buy btn btn-info">立即購買</div>
            </div>
            </form>

        </div>
    </div>
</section>

@endsection

@section('js')

<script>
    $('.card-box *').attr('style','');

    $('.color .cart-buttons ').click(function(){

        $('.color .cart-buttons').removeClass("active")
        $(this).addClass("active");

        var color = $(this).attr("data-color");

        $('#color').val(color);
    });

    $(' .specification .cart-buttons').click(function(){

        $('.specification .cart-buttons').removeClass("active")
        $(this).addClass("active");

        var specification = $(this).attr("data-specification");

        $('#specification').val(specification);
    });



    $(function(){
        $('.add').click(function () {
		  if ($(this).prev().val() < 20) {
    	    $(this).prev().val(+$(this).prev().val() + 1);
		}

        $('.sub').click(function () {
		  if ($(this).next().val() > 1) {
    	    if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
		}
});
});
    })


</script>

@endsection

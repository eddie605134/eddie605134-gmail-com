@extends('layouts/nav')



@section('content')

<section class="services1 cid-rSHipPxh3m" id="services1-5">
    <!---->

    <!---->
    <!--Overlay-->

    <!--Container-->
    <div class="container" style="margin-top:80px;">
        <div class="row ">



            @foreach ($product_data as $item)
            <div class="card  col-md-6 p-3 col-lg-4">
                <div class="card">
                    <div class="card-img">
                        <img src="{{$item->p_img}}" alt="product">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-5">
                            {{$item->title}}
                        </h4>

                        <!--Btn-->
                        <div class="mbr-section-btn align-left">
                        <a href="/product_details/{{$item->id}}" class="btn btn-warning-outline display-4">
                                NT${{$item->price}}
                            </a>
                        </div>
                    </div>

                </div>

            </div>
            @endforeach





            <!--Card-2-->

            <!--Card-4-->

        </div>
    </div>
</section>
@endsection

@extends('layouts/app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')


<div class="container">

    <a href="/home/news/create" class="btn btn-success">最新資料更新</a>
<hr>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>img</th>
                <th>title</th>
                <th>content</th>
                <th>sort</th>
                <th>修改/刪除</th>
            </tr>
        <thead>
        <tbody>

            @foreach ($all_news as $item)

            <tr  >
                <td><img src="{{$item->img}}" alt="" width="100px">
                    </td>
                <td>{{$item->title}}</td>
                <td>{!!$item->content!!}</td>
                <td>{{$item->sort}}</td>
                <td >
                    <a href="/home/news/edit/{{$item->id}}" class="btn btn-success btn-sm">修改</a>
                    <a  class="btn btn-danger btn-sm" onclick="show_confirm({{$item->id}})">刪除</a>
                <form id="logout-form-{{$item->id}}" action="/home/news/delete/{{$item->id}}" method="POST" style="display: none;"> @csrf </form>
                    {{-- 路徑放在action --}}
                </td>
            </tr>

            @endforeach


        </tbody>
        {{-- <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>

            </tr>
        </tfoot> --}}
    </table>
</div>
@endsection


@section('js')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {


    $('#example').dataTable( {"order": [[ 3, 'desc' ]]} );
} );

function show_confirm(id){
  var r=confirm("刪掉就沒了")
  if (r==true){
      //使用者確認刪除
    document.getElementById(`logout-form-${id}`).submit();
    }
    // function show_confirm(id){
    //         var r=confirm("你確定要刪除嗎!");
    //         if (r==true){
    //             //使用者確認刪除
    //             document.getElementById(`delete-form-${id}`).submit();
    //         }
    //     }
  }
</script>
@endsection



@extends('layouts/app')


@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
<style>
    .news_img_card .btn-danger {
        border-radius: 50%;
        position: absolute;
        right: -5px;
        top: -5px;
    }
</style>
@endsection

@section('content')


<div class="container">

    <form method="POST" action="/home/news/update/{{$news->id}}" multiple="multiple" enctype="multipart/form-data">
        <h1>我是UPDATE</h1>
        @csrf
        <div class="form-group">
            <label for="img1">現有圖片</label>
            <img src="{{$news->img}}" alt="" width="100px">

        </div>
        <div class="form-group">
            <label for="img1">主要圖片上傳</label>
            <input type="file" class="form-control" id="img" aria-describedby="emailHelp" placeholder="Enter email"
                name="img">

        </div>

        <hr>
        <div class="row">
            <label for="imgs">現有多張圖片組</label>
            @foreach ($news->news_imgs as $item)
            <div class=" col-2 ">
                <div class="news_img_card" data-newsimgid="{{$item->id}}">
                    <button type="button" class="btn btn-danger" data-newsimgid="{{$item->id}}">X</button>
                    <img class="img-fluid" src="{{$item->img_url}}" alt="">
                    <input class="form-controll" type="text" value="{{$item->sort}}"
                        onchange="ajax_post_sort(this,{{$item->id}})">
                </div>
            </div>
            @endforeach
        </div>
        <div class="form-group">
            <label for="imgs">多張圖片組上傳</label>
            <input type="file" class="form-control" id="news_imgs" aria-describedby="emailHelp"
                placeholder="Enter email" name="news_imgs[]" required multiple>

            <hr>


            <div class="form-group">
                <label for="title">標題</label>
                <input type="text" class="form-control" id="title" placeholder="title" name="title"
                    value="{{$news->title}}">
            </div>

            <div class="form-group">
                <label for="content">內文</label>
                <textarea type="text" class="form-control" id="content" placeholder="content" name="content" cols="30"
                    rows="10">{!!$news->content!!}</textarea>
            </div>

            <div class="form-group">
                <label for="sort">sort</label>
                <input type="text" class="form-control" id="sort" placeholder="sort" name="sort"
                    value="{{$news->sort}}">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>


@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
<script src="{{asset('js/summernote-zh-TW.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.news_img_card .btn-danger').click(function(){

        var newsimgid = this.getAttribute('data-newsimgid')
    $.ajax({
              url: "/home/ajax_delete_new_imgs",
              method: 'post',
              data: {
                newsimgid: newsimgid,
              },
              success: function(result){
                 $(`.news_img_card[data-newsimgid=${newsimgid}]`).remove();
                 }
                });

    });

    function ajax_post_sort(element,img_id){
        var img_id;
        var sort_value = element.value;

        $.ajax({
              url: "/home/ajax_post_sort",
              method: 'post',
              data: {
                id: img_id,
                sort: sort_value,
              },
              success: function(result){
                  console.log(result);
                 }
                });

    };

    $(document).ready(function() {
  $('#content').summernote({
    minHeight: 300,
    lang:'zh-TW'
  });
});


</script>
@endsection

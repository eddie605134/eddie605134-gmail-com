@extends('layouts/app')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')


<div class="container">

<form method="POST" action="/home/news/store" enctype="multipart/form-data" multiple="multiple">
    @csrf
    <div class="form-group">
      <label for="img1">主要圖片上傳</label>
      <input type="file" class="form-control" id="img" aria-describedby="emailHelp" placeholder="Enter email" name="img" required >

    </div>

    <div class="form-group">
        <label for="news_imgs">多張圖片上傳</label>
        <input type="file" class="form-control" id="news_imgs" aria-describedby="emailHelp" placeholder="Enter email" name="news_imgs[]" required multiple>

      </div>

    <div class="form-group">
      <label for="title">標題</label>
      <input type="text" class="form-control" id="title" placeholder="title" name="title" required>
    </div>

    <div class="form-group">
        <label for="content">內文</label>
        <textarea type="text" class="form-control" id="content" placeholder="content" name="content" required></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>


@endsection


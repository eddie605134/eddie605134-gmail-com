@extends('layouts/app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')


<div class="container">

    <a href="/home/producttype/create" class="btn btn-success">新增產品類別</a>
<hr>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>type_name</th>
                <th>sort</th>
                <th>修改/刪除</th>
            </tr>
        <thead>
        <tbody>

            @foreach ($all_producttype as $item)

            <tr  >
                <td>{{$item->type_name}}</td>
                <th>{{$item->sort}}</th>
                <td >
                    <a href="/home/producttype/edit/{{$item->id}}" class="btn btn-success btn-sm">修改</a>
                    <a  class="btn btn-danger btn-sm" onclick="show_confirm({{$item->id}})">刪除</a>
                <form id="logout-form-{{$item->id}}" action="/home/producttype/delete/{{$item->id}}" method="POST" style="display: none;"> @csrf </form>

                </td>
            </tr>

            @endforeach


        </tbody>
        {{-- <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>

            </tr>
        </tfoot> --}}
    </table>
</div>
@endsection


@section('js')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {


    $('#example').dataTable( {"order": [[ 1, 'desc' ]]} );
} );

function show_confirm(id){
  var r=confirm("刪掉就沒了")
  if (r==true){
      //使用者確認刪除
    document.getElementById(`logout-form-${id}`).submit();
    }

  }
</script>
@endsection

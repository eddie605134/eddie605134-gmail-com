@extends('layouts/app')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')


<div class="container">

<form method="POST" action="/home/producttype/store"  >
    @csrf
    <div class="form-group">
      <label for="type_name">類型名稱</label>
      <input type="text" class="form-control" id="type_name" aria-describedby="emailHelp" placeholder="Enter name" name="type_name" required >

    </div>



    <div class="form-group">
      <label for="sort">sort</label>
      <input type="text" class="form-control" id="sort" placeholder="sort" name="sort" required>
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>


@endsection

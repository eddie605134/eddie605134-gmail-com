@extends('layouts/app')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')


<div class="container">

    <form method="POST" action="/home/product/update/{{$product->id}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="img1">現有圖片</label>
            <img src="{{$product->p_img}}" alt="" width="100px">

        </div>

        <div class="form-group">
            <label for="p_img">商品圖片上傳</label>
            <input type="file" class="form-control" id="p_img" aria-describedby="emailHelp" placeholder="Enter email"
                name="p_img" required></option>

        </div>

        <hr>



        <div class="form-group">
            <label for="exampleFormControlSelect1">類別選擇</label>
            <select class="form-control" id="exampleFormControlSelect1" name="product_type">
                @foreach ($producttype as $item)
                @if ($product->product_type == $item->type_name)
                <option value="{{$item->type_name}}" selected>{{$item->type_name}}</option>
                @else
                <option value="{{$item->type_name}}">{{$item->type_name}}</option>
                @endif
                @endforeach


            </select>
        </div>

        <div class="form-group">
            <label for="title">產品名</label>
            <input type="text" class="form-control" id="title" placeholder="預設值為0" name="title"
                value="{{$product->title}}" required></option>
        </div>

        <div class="form-group">
            <label for="sort">價格</label>
            <input type="text" class="form-control" id="price" placeholder="輸入修改價格" name="price" value="{{$product->price}}"
                required></option>
        </div>

        <div class="form-group">
            <label for="sort">權重</label>
            <input type="text" class="form-control" id="sort" placeholder="預設值為0" name="sort" value="{{$product->sort}}"
                required></option>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>


@endsection

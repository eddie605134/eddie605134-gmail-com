@extends('layouts/app')

@section('css')
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')


<div class="container">

    <form method="POST" action="/home/product/store" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="p_img">商品圖片上傳</label>
            <input type="file" class="form-control" id="p_img" aria-describedby="emailHelp" placeholder="Enter email"
                name="p_img" required></option>

        </div>

        <div class="form-group">
            <label for="exampleFormControlSelect1">類別選擇</label>
            <select class="form-control" id="exampleFormControlSelect1" name="product_type">
                @foreach ($all_producttype as $item)
                <option>{{$item->type_name}}</option>
                @endforeach


            </select>
        </div>

        <div class="form-group">
            <label for="title">產品名</label>
            <input type="text" class="form-control" id="title" placeholder="預設值為0" name="title" required></option>
        </div>

        <div class="form-group">
            <label for="sort">價格</label>
            <input type="text" class="form-control" id="pruce" placeholder="輸入商品價格" name="price" required></option>
        </div>

        <div class="form-group">
            <label for="sort">權重</label>
            <input type="text" class="form-control" id="sort" placeholder="預設值為0" name="sort" required></option>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>


@endsection
